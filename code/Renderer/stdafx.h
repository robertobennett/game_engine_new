#ifndef RENDERER_DLL_STDAFX_H
#define RENDERER_DLL_STDAFX_H

#include <Windows.h>
#include <stdlib.h>
#include <string>
#include <map>
#include <vector>
#include <array>
#include <mutex>

#ifdef RENDERER_DLL_EXPORT
#define RENDERER_DLL_IMPORT_EXPORT __declspec(dllexport)
#else
#define RENDERER_DLL_IMPORT_EXPORT __declspec(dllimport)
#endif

#ifdef CORE_DLL_EXPORT
#define CORE_DLL_IMPORT_EXPORT __declspec(dllexport)
#else
#define CORE_DLL_IMPORT_EXPORT __declspec(dllimport)
#endif

#endif