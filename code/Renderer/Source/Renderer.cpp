#include "stdafx.h"
#include "Renderer.h"

CRenderer::CRenderer() :
m_bInitialized(false),
m_pDirect3D(nullptr)
{
}

CRenderer::~CRenderer()
{
}

bool CRenderer::Init(HWND hWnd)
{
	if (!m_bInitialized)
	{
		m_pDirect3D = new CDirect3D();
		if (!m_pDirect3D->Initialize(hWnd))
			return false;

		CXmlReader reader;
		reader.Parse("C:\\Programming\\Game_Engine\\settings\\engine.xml");

		m_bInitialized = true;
		return true;
	}

	return false;
}

void CRenderer::Update()
{
	m_pDirect3D->Update();
}

void CRenderer::Resize()
{
	m_pDirect3D->Resize();
}

void CRenderer::Shutdown()
{
	m_pDirect3D->Shutdown();
	delete m_pDirect3D;
	m_pDirect3D = nullptr;

	return;
}
