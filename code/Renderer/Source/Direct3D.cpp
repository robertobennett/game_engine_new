#include "stdafx.h"
#include "Direct3D.h"

std::mutex g_UpdateResizeMutex;

CDirect3D::CDirect3D() :
m_bInitialized(false),
m_pD3D11Device(nullptr),
m_pD3D11DeviceContext(nullptr),
m_pDXGISwapChain(nullptr),
m_pD3D11RenderTargetView(nullptr)
{
}

CDirect3D::~CDirect3D()
{
	Shutdown();
}

bool CDirect3D::Initialize(HWND hWnd)
{
	if (!m_bInitialized)
	{
		HRESULT hRes = S_OK;
		m_hWnd = hWnd;

		hRes = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, NULL, 0, D3D11_SDK_VERSION, &m_pD3D11Device, NULL, &m_pD3D11DeviceContext);
		if (FAILED(hRes))
			return false;

		IDXGIDevice* pDxgiDevice = nullptr;
		hRes = m_pD3D11Device->QueryInterface(__uuidof(IDXGIDevice), (void**)&pDxgiDevice);
		if (FAILED(hRes))
			return false;

		IDXGIAdapter* pDxgiAdapter = nullptr;
		hRes = pDxgiDevice->GetAdapter(&pDxgiAdapter);
		if (FAILED(hRes))
		{
			pDxgiDevice->Release();
			pDxgiDevice = nullptr;

			return false;
		}

		pDxgiDevice->Release();
		pDxgiDevice = nullptr;

		IDXGIFactory* pDxgiFactory = nullptr;
		hRes = pDxgiAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&pDxgiFactory);
		if (FAILED(hRes))
		{
			pDxgiAdapter->Release();
			pDxgiAdapter = nullptr;

			return false;
		}

		RECT rect; ZeroMemory(&rect, sizeof(RECT));
		GetClientRect(m_hWnd, &rect);

		DXGI_SWAP_CHAIN_DESC scd; ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));
		scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scd.BufferDesc.Height = rect.bottom - rect.top;
		scd.BufferDesc.Width = rect.right - rect.left;
		scd.BufferDesc.RefreshRate.Numerator = 0;
		scd.BufferDesc.RefreshRate.Denominator = 1;
		scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		scd.BufferCount = 1;
		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.OutputWindow = m_hWnd;
		scd.SampleDesc.Count = 1;
		scd.SampleDesc.Quality = 0;
		scd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		scd.Windowed = true;

		hRes = pDxgiFactory->CreateSwapChain(m_pD3D11Device, &scd, &m_pDXGISwapChain);
		if (FAILED(hRes))
		{
			pDxgiFactory->Release();
			pDxgiFactory = nullptr;

			pDxgiDevice->Release();
			pDxgiDevice = nullptr;

			return false;
		}

		pDxgiFactory->Release();
		pDxgiFactory = nullptr;

		ID3D11Texture2D* pD3D11BackBuffer = nullptr;
		hRes = m_pDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&pD3D11BackBuffer);
		if (FAILED(hRes))
			return false;

		hRes = m_pD3D11Device->CreateRenderTargetView(pD3D11BackBuffer, NULL, &m_pD3D11RenderTargetView);
		if (FAILED(hRes))
		{
			pD3D11BackBuffer->Release();
			pD3D11BackBuffer = nullptr;
			return false;
		}

		pD3D11BackBuffer->Release();
		pD3D11BackBuffer = nullptr;

		m_pD3D11DeviceContext->OMSetRenderTargets(1, &m_pD3D11RenderTargetView, nullptr);

		D3D11_VIEWPORT vp; ZeroMemory(&vp, sizeof(D3D11_VIEWPORT));
		vp.Height = rect.bottom - rect.top;
		vp.Width = rect.right - rect.left;
		vp.MaxDepth = 0.0f;
		vp.MinDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;

		m_pD3D11DeviceContext->RSSetViewports(1, &vp);

		std::array<float, 4> rgba{ 0.0f, 0.0f, 0.0f, 1.0f };
		CCore::GetInstance()->SharedMemory()->CreateVariable("ClearColour", rgba);

		m_bInitialized = true;
		return true;
	}

	return false;
}

void CDirect3D::Update()
{
	std::lock_guard<std::mutex> guard(g_UpdateResizeMutex);
	m_pD3D11DeviceContext->ClearRenderTargetView(m_pD3D11RenderTargetView, CCore::GetInstance()->SharedMemory()->FetchVariable<std::array<float, 4>>("ClearColour")->data());

	m_pDXGISwapChain->Present(0, 0);
}

void CDirect3D::Resize()
{
	std::lock_guard<std::mutex> guard(g_UpdateResizeMutex);
	HRESULT hRes = S_OK;

	m_pD3D11DeviceContext->OMSetRenderTargets(0, 0, 0);

	m_pD3D11RenderTargetView->Release();
	m_pD3D11RenderTargetView = nullptr;

	RECT rc; ZeroMemory(&rc, sizeof(RECT));
	GetClientRect(m_hWnd, &rc);
	hRes = m_pDXGISwapChain->ResizeBuffers(0, rc.right - rc.left, rc.bottom - rc.top, DXGI_FORMAT_UNKNOWN, 0);
	if (FAILED(hRes))
		throw;
		
	ID3D11Texture2D* pD3D11BackBuffer = nullptr;

	hRes = m_pDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&pD3D11BackBuffer);

	if (FAILED(hRes))
		throw;

	hRes = m_pD3D11Device->CreateRenderTargetView(pD3D11BackBuffer, NULL, &m_pD3D11RenderTargetView);

	if (FAILED(hRes))
		throw;

	pD3D11BackBuffer->Release();
	pD3D11BackBuffer = nullptr;

	m_pD3D11DeviceContext->OMSetRenderTargets(1, &m_pD3D11RenderTargetView, nullptr);

	D3D11_VIEWPORT vp; ZeroMemory(&vp, sizeof(D3D11_VIEWPORT));
	vp.Height = rc.bottom - rc.top;
	vp.Width = rc.right - rc.left;
	vp.MaxDepth = 1.0f;
	vp.MinDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;

	m_pD3D11DeviceContext->RSSetViewports(1, &vp);
}

void CDirect3D::Shutdown()
{
	if (m_bInitialized)
	{
		CCore::GetInstance()->SharedMemory()->DestroyVariable("ClearColour");

		if (m_pD3D11RenderTargetView != nullptr)
		{
			m_pD3D11RenderTargetView->Release();
			m_pD3D11RenderTargetView = nullptr;
		}

		if (m_pDXGISwapChain != nullptr)
		{
			m_pDXGISwapChain->Release();
			m_pDXGISwapChain = nullptr;
		}

		if (m_pD3D11DeviceContext != nullptr)
		{
			m_pD3D11DeviceContext->Release();
			m_pD3D11DeviceContext = nullptr;
		}

		if (m_pD3D11Device != nullptr)
		{
			m_pD3D11Device->Release();
			m_pD3D11Device = nullptr;
		}

		m_bInitialized = false;
	}
}