#ifndef RENDERER_DLL_RENDERER_H
#define RENDERER_DLL_RENDERER_H

#include "Direct3D.h"

#include <Core/Include/XmlReader.h>

class RENDERER_DLL_IMPORT_EXPORT CRenderer
{
	public:
		CRenderer();
		~CRenderer();

		bool Init(HWND hWnd);
		void Update();
		void Resize();
		void Shutdown();

	private:
		bool m_bInitialized;
		CDirect3D* m_pDirect3D;
};

#endif