#pragma once
#ifndef RENDERER_DLL_DIRECT_3D
#define RENDERER_DLL_DIRECT_3D

#include <Core\Include\Core.h>

#include <d3d11.h>

class RENDERER_DLL_IMPORT_EXPORT CDirect3D
{
	public:
		CDirect3D();
		~CDirect3D();

		bool Initialize(HWND hWnd);
		void Update();
		void Resize();
		void Shutdown();

	private:
		bool m_bInitialized;

		HWND m_hWnd;
		ID3D11Device* m_pD3D11Device;
		ID3D11DeviceContext* m_pD3D11DeviceContext;
		IDXGISwapChain* m_pDXGISwapChain;
		ID3D11RenderTargetView* m_pD3D11RenderTargetView;
};

#endif