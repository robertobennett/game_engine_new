#include "stdafx.h"
#include "Core.h"

CCore* CCore::sm_pInstance = CCore::GetInstance();

CCore::CCore()
{
	m_pSharedMemory = new CSharedMemory();
}

CCore::~CCore()
{
	delete m_pSharedMemory;
	m_pSharedMemory = nullptr;
}