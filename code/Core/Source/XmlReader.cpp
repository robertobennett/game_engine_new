#include "stdafx.h"
#include "XmlReader.h"

CXmlReader::CXmlReader() :
m_pRootElement(nullptr)
{
}

CXmlReader::~CXmlReader()
{
}

bool CXmlReader::Parse(std::string sFile)
{
	std::ifstream file(sFile, std::ifstream::in);
	if (file.is_open())
	{
		uint64_t ullFileLen = 0;
		file.seekg(0, file.end);
		ullFileLen = file.tellg();
		file.seekg(0, file.beg);

		char* pFileBuff = new char[ullFileLen];
		file.read(pFileBuff, ullFileLen);

		std::string sFile(pFileBuff);

		delete[] pFileBuff;
		pFileBuff = nullptr;

		size_t szCommentStartPos = sFile.find("<!--", 0);
		while (szCommentStartPos != std::string::npos)
		{
			size_t szCommentEndPos = sFile.find("-->", szCommentStartPos);

			sFile = sFile.erase(szCommentStartPos, (szCommentEndPos + 3) - szCommentStartPos);

			szCommentStartPos = sFile.find("<!--", szCommentStartPos);
		}

		return true;
	}

	return false;
}
