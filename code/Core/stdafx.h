#ifndef CORE_DLL_STDAFX_H
#define CORE_DLL_STDAFX_H

#include <Windows.h>
#include <stdlib.h>
#include <string>
#include <map>
#include <vector>

#ifdef CORE_DLL_EXPORT
#define CORE_DLL_IMPORT_EXPORT __declspec(dllexport)
#else
#define CORE_DLL_IMPORT_EXPORT __declspec(dllimport)
#endif

#endif