#pragma once
#ifndef CORE_DLL_SHARED_MEMORY_H
#define CORE_DLL_SHARED_MEMORY_H

class CORE_DLL_IMPORT_EXPORT CSharedMemory
{
	private:
		struct SharedVarBase
		{
			virtual ~SharedVarBase() { return; }
			std::string sName;
		};

		template<class T>
		struct SharedVar : SharedVarBase
		{
			public:
				SharedVar(T initialiValue)
				{
					var = initialiValue;
				}

				T var;

			private:
				SharedVar() { return; }
		};

	public:
		CSharedMemory();
		~CSharedMemory();

		template<class T>
		T* const CreateVariable(std::string sName, T initialValue)
		{
			if (m_SharedVars.count(sName) == 0)
			{
				m_SharedVars[sName] = (SharedVarBase*)(new SharedVar<T>(initialValue));
				return &((SharedVar<T>*)m_SharedVars[sName])->var;
			}
		}

		template<class T>
		T* const FetchVariable(std::string sName)
		{
			return &((SharedVar<T>*)m_SharedVars[sName])->var;
		}

		void DestroyVariable(std::string sName)
		{
			delete m_SharedVars[sName];
			m_SharedVars.erase(sName);
		}

	private:
		std::map<std::string, SharedVarBase*> m_SharedVars;
};

#endif