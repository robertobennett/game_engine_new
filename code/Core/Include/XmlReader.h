#pragma once
#ifndef CORE_DLL_XML_PARSER_H
#define CORE_DLL_XML_PARSER_H

#include <fstream>

class CORE_DLL_IMPORT_EXPORT CXmlReader
{
	private:
		struct Element
		{
			~Element()
			{
				if (pChildren.size() > 0)
				{
					for (UINT i = 0; i < pChildren.size(); i++) 
					{
						delete pChildren[i];
						pChildren[i] = nullptr;
					}
				}
			}

			std::string sName;
			std::vector<Element*> pChildren;
			std::string sValue;
		};

	public:
		CXmlReader();
		~CXmlReader();

		bool Parse(std::string sFile);

	private:
		Element * m_pRootElement;
};

#endif