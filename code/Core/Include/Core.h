#pragma once
#ifndef CORE_DLL_CORE_H
#define CORE_DLL_CORE_H

#include "SharedMemory.h"

class CORE_DLL_IMPORT_EXPORT CCore
{
	public:
		static CCore * GetInstance()
		{
			if (sm_pInstance == nullptr)
				sm_pInstance = new CCore();

			return sm_pInstance;
		}

		CSharedMemory * const SharedMemory() const
		{
			return m_pSharedMemory;
		}

	private:
		CCore();
		~CCore();
		static CCore* sm_pInstance;

		CSharedMemory* m_pSharedMemory;
};

#endif