#include "stdafx.h"
#include "Editor_Wrapper.h"

EditorWrapper::CEditorWrapper::CEditorWrapper() :
m_bInitialized(false),
m_pRenderer(nullptr)
{
}

EditorWrapper::CEditorWrapper::~CEditorWrapper()
{
	Shutdown();
}

EditorWrapper::CEditorWrapper::!CEditorWrapper()
{
	Shutdown();
}

bool EditorWrapper::CEditorWrapper::Init(IntPtr^ hWnd)
{
	if (!m_bInitialized)
	{
		m_pRenderer = new CRenderer();
		if (!m_pRenderer->Init((HWND)hWnd->ToPointer()))
			return false;

		m_bInitialized = true;
		return true;
	}

	return false;
}

void EditorWrapper::CEditorWrapper::Update()
{
	m_pRenderer->Update();
}

void EditorWrapper::CEditorWrapper::Resize()
{
	m_pRenderer->Resize();
}

void EditorWrapper::CEditorWrapper::Shutdown()
{
	if (m_pRenderer != nullptr)
	{
		m_pRenderer->Shutdown();
		delete m_pRenderer;
		m_pRenderer = nullptr;
	}
}

void EditorWrapper::CEditorWrapper::SetClearColour(float^ R, float^ G, float^ B)
{
	std::array<float, 4> rgba{ (float)R, (float)G, (float)B, 1.0f };
	*CCore::GetInstance()->SharedMemory()->FetchVariable<std::array<float, 4>>("ClearColour") = rgba;

}

void EditorWrapper::CEditorWrapper::SetClearColour(float^ R, float^ G, float^ B, float^ A)
{
	std::array<float, 4> rgba{ (float)R, (float)G, (float)B, (float)A };
	*CCore::GetInstance()->SharedMemory()->FetchVariable<std::array<float, 4>>("ClearColour") = rgba;

}