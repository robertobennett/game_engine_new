#ifndef EDITOR_WRAPPER_DLL_EDITOR_WRAPPER_H
#define EDITOR_WRAPPER_DLL_EDITOR_WRAPPER_H

#include <Renderer/Include/Renderer.h>

#include <Core/Include/Core.h>

using namespace System;

namespace EditorWrapper
{
	public ref class CEditorWrapper
	{
		public:
			CEditorWrapper();
			~CEditorWrapper();
			!CEditorWrapper();

			bool Init(IntPtr^ hWnd);
			void Update();
			void Resize();
			void Shutdown();

			void SetClearColour(float^ R, float^ G, float^ B);
			void SetClearColour(float^ R, float^ G, float^ B, float^ A);

		private:
			bool m_bInitialized;
			CRenderer* m_pRenderer;
	};
}

#endif