﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EditorWrapper;

namespace Editor
{
    public partial class MainWindow : Window
    {
        private Thread _UpdateLoopThread = null;
        private CEditorWrapper _Editor = null;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            _UpdateLoopThread.Abort();
            _UpdateLoopThread = null;
            _Editor.Dispose();
            _Editor = null;
        }

        private void RenderTarget_Loaded(object sender, RoutedEventArgs e)
        {
            _Editor = new CEditorWrapper();
            if (!_Editor.Init(RenderTarget.RenderTargetHandle))
                throw new Exception("Failed to initialize editor");

            Color rgba = (Color)clrpck.SelectedColor;
            _Editor.SetClearColour(rgba.R / 255.0f, rgba.G / 255.0f, rgba.B / 255.0f, rgba.A / 255.0f);

            _UpdateLoopThread = new Thread(new ParameterizedThreadStart(this.UpdateLoop));
            _UpdateLoopThread.Name = "Update Loop";
            _UpdateLoopThread.Start(_Editor);
            //ComponentDispatcher.ThreadIdle += ComponentDispatcher_ThreadIdle;
        }

        //private void ComponentDispatcher_ThreadIdle(object sender, EventArgs e)
        //{
        //    _Editor.Update();
        //}

        private bool RenderTarget_MessageHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam)
        {
            switch (msg)
            {
                default:
                    return false;

                //WM_SIZE
                case 0x0005:
                    _Editor.Resize();
                    break;
            }

            return true;
        }

        private void ClrPcker_Background_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            if (_Editor != null)
            {
                Color rgba = (Color)e.NewValue;
                _Editor.SetClearColour(rgba.R / 255.0f, rgba.G / 255.0f, rgba.B / 255.0f, rgba.A / 255.0f);
            }
        }

        private void UpdateLoop(object editor)
        {
            while (true)
            {
                _Editor.Update();
            }
        }
    }
}
