﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;

namespace Editor.Controls
{
    public class RenderTarget : Border
    {
        private RenderTargetHost _RenderTargetHost = null;
        public static readonly DependencyProperty RenderTargetNameProperty = DependencyProperty.Register("RenderTargetName", typeof(string), typeof(RenderTarget), new PropertyMetadata("default"));
        public string RenderTargetName
        {
            get
            {
                return (string)GetValue(RenderTargetNameProperty);
            }
            set
            {
                if (value.Length <= 0)
                    value = "default";

                SetValue(RenderTargetNameProperty, value);
            }
        }

        public delegate bool WndProcDelegate(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam);
        public event WndProcDelegate MessageHook;
        private IntPtr WndProcRelay(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            handled = MessageHook.Invoke(hwnd, msg, wParam, lParam);

            return IntPtr.Zero;
        }

        public RenderTarget()
        {
            this.Loaded += RenderTarget_Loaded;
        }

        private void RenderTarget_Loaded(object sender, RoutedEventArgs e)
        {
            if ((int)this.ActualWidth <= 0 || (int)this.ActualHeight <= 0)
                throw new Exception("Invalid render target dimensions (width/height) " + this.ActualWidth + "/" + this.ActualHeight);

            _RenderTargetHost = new RenderTargetHost((int)this.ActualWidth, (int)this.ActualHeight, RenderTargetName);
            this.Child = _RenderTargetHost;

            _RenderTargetHost.MessageHook += WndProcRelay;
        }

        private class RenderTargetHost : HwndHost
        {
            #region Win32Imports

            internal const int WS_CHILD = unchecked(0x40000000), WS_VISIBLE = unchecked(0x10000000), WM_SIZE = 0x0005;

            [DllImport("kernel32.dll")]
            public static extern IntPtr GetModuleHandle(string lpModuleName);

            [DllImport("user32.dll", SetLastError = true, EntryPoint = "CreateWindowEx")]
            public static extern IntPtr CreateWindowEx(
               int dwExStyle,
               string lpClassName,
               string lpWindowName,
               int dwStyle,
               int x,
               int y,
               int nWidth,
               int nHeight,
               IntPtr hWndParent,
               IntPtr hMenu,
               IntPtr hInstance,
               IntPtr lpParam);

            [DllImport("user32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Unicode)]
            internal static extern int SendMessage(IntPtr hwnd,
                                       int msg,
                                       IntPtr wParam,
                                       IntPtr lParam);

            [DllImport("user32.dll", SetLastError = true, EntryPoint = "DestroyWindow")]
            public static extern IntPtr DestroyWindow(
               IntPtr hWndParent);
            #endregion

            private IntPtr _Hwnd = IntPtr.Zero;
            public IntPtr Hwnd
            {
                get
                {
                    return _Hwnd;
                }
            }

            private string _wndName;
            private int _width, _height;

            public RenderTargetHost(int width, int height, string wndName)
            {
                _width = width;
                _height = height;
                _wndName = wndName;
            }

            protected override HandleRef BuildWindowCore(HandleRef hwndParent)
            {
                _Hwnd = CreateWindowEx(0, "static", _wndName, WS_CHILD | WS_VISIBLE, 0, 0, _width, _height, hwndParent.Handle, IntPtr.Zero, GetModuleHandle(""), IntPtr.Zero);
                if (_Hwnd == IntPtr.Zero)
                    throw new Exception("Couldn't create RenderTargetHost");

                return new HandleRef(this, _Hwnd);
            }

            protected override IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
            {
                switch (msg)
                {
                    default:
                        handled = false;
                        break;
                }

                return IntPtr.Zero;
            }

            protected override void DestroyWindowCore(HandleRef hwnd)
            {
                DestroyWindow(_Hwnd);
            }
        }
    }
}
