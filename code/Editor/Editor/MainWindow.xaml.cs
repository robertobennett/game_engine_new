﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Editor
{
    public partial class MainWindow : Window
    {
        private Thread _UpdateLoopThread = null;
        private EditorWrapper _Editor = null;
        public MainWindow()
        {
            InitializeComponent();
        }

        private bool RenderTarget_MessageHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam)
        {
            return false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _UpdateLoopThread = new Thread(new ParameterizedThreadStart(UpdateLoop));
            _UpdateLoopThread.Name = "Update Loop";
            _UpdateLoopThread.Start(_Editor);
        }

        private void UpdateLoop(EditorWrapper editor)
        {
            /*while (editor.state != editor.states.exit)
            {
                editor.update();
            }*/
        }
    }
}
